import React from 'react';
import blend_colors from './blend_colors';

interface Props
{
    stars,
    selected,
    onSelected(value),
}
interface State
{
    dynamicSelectedIndex,
}

/**
 * @author Germán Martínez
 */
class Rating extends React.Component<Props,State>
{
    state: State =
    {
        dynamicSelectedIndex: 0
    }

    render()
    {
        let stars: {
            isFilled,
        }[] = [];

        for(let i = 0; i < this.props.stars; i++)
        {
            stars.push({
                isFilled: this.isFilled(i+1)
            });
        }
        return (
            <div className="rating">
                { stars.map((star, index) =>
                {
                    const style: any = {
                        color: !(this.isColorsEnabled())
                                ? undefined
                                :
                                blend_colors('#F44336', '#FFEB3B', ((index+1)*100)/(this.props.stars)/100)
                    };
                    return (
                        <div
                            key={index}
                            className="rating-star"
                            onClick={() => this.handleClick(index)}
                            onMouseEnter={() => this.handleMouseEnter(index)}
                            onMouseLeave={() => this.handleMouseLeave(index)}
                            style={style}
                        >
                            { star.isFilled ? "★" : "☆" }
                        </div>
                    );
                })}
            </div>
        );
    }

    handleClick = (index) =>
    {
        this.props.onSelected(index+1);
    }

    handleMouseEnter = (index) =>
    {
        this.setState({
            dynamicSelectedIndex: index+1
        });
    }

    handleMouseLeave = (index) =>
    {
        this.setState({
            dynamicSelectedIndex: 0
        });
    }

    isFilled = (index) =>
    {
        if(this.state.dynamicSelectedIndex > 0)
        {
            return (index > 0 && index <= this.state.dynamicSelectedIndex);
        }
        if(this.props.selected > 0)
        {
            return (index > 0 && index <= this.props.selected);
        }
        return false;
    }

    isColorsEnabled = () =>
    {
        return (
            (typeof this.props.selected === 'number' && this.props.selected > 0)
            ||
            (typeof this.state.dynamicSelectedIndex === 'number' && this.state.dynamicSelectedIndex > 0));
    }
};
export default Rating;

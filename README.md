# Germix React Core - Rating

## About

Germix react core rating component

## Installation

```bash
npm install @germix/germix-react-core-rating
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```

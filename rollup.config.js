import copy from 'rollup-plugin-copy';
import typescript from 'rollup-plugin-typescript2';

export default {
  input: 'src/index.tsx',
  output: {
    file: 'dist/index.js',
    format: 'cjs'
  },
  plugins: [
    typescript({
      typescript: require('typescript'),
      objectHashIgnoreUnknownHack: true,
    }),
    copy({
      targets: [
        { src: 'src/styles', dest: 'dist' },
        { src: 'src/index.scss', dest: 'dist' },
      ]
    })
  ]
}
